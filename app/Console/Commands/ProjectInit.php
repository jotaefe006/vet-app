<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ProjectInit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'project:init';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Inicializar proyecto';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Creando .env si este no existe.');
        if(!file_exists('.env')){
            copy('.env.example','.env');

        }

        file_exists('.env')

            ? $this->info('Exito')
            : $this->info('Error');
            $this->info('Generando la llave del projecto.');
            $this->call('key:generate');

            return 0;

    }
}
