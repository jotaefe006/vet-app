<form action="{{url('/owner')}}" method="post" enctype="multipart/form-data">
    @csrf
    <label for="name">Nombre</label>
    <input type="text" name="name" id="name">
    <label for="address">Dirección</label>
    <input type="text" name="address" id="address">
    <label for="phone">Teléfono</label>
    <input type="text" name="phone" id="phone">
    <input type="submit" value="Guardar">
</form>
